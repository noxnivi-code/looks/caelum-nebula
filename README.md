# Caelum-Nebula

### Project Status
As all the elements of the **NoxNivi** 'set', this is a Work In Progress,
therefore development pace is slow.

## Description
Default Icons for the NoxNivi set.
Intended to be used with dark themes. NoxNivi default theme is black thus this
fits.

## Screenshots
![Caelum-Nebula icon theme](/screenshots/caelum-nebula.png "Caelum-Nebula")*Caelum-Nebula*

## Installation
**General instructions**

Download the the compressed package, decompress it and copy the Caelum-Nebula
folder to ~/.icons/
Then select the Caelum-Nebula theme in your cursor preference settings.

**Arch Linux**

If user is running Arch or any Arch derived distro, can add the NoxNivi
repository and install it using `pacman`:
```
sudo pacman -S caelum-nebula
```
Then select the Caelum-Nebula icon theme in your cursor preference settings.


## Usage
Install, select in icon theme preferences and enjoy.

## Support

## License
THis software is released under the GPLv3 license. A copy of said license is provided in this folder.
